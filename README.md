## Test Git LFS migration

References:

- Similar guide on BitBucket: https://confluence.atlassian.com/bitbucket/use-bfg-to-migrate-a-repo-to-git-lfs-834233484.html
- For https://gitlab.com/gitlab-org/gitlab-ce/issues/58911
- BFG instructions: https://rtyley.github.io/bfg-repo-cleaner/

## Steps

1. Back up your repository:

   Create a copy of your repository so that you can
   recover it in case something goes wrong.

1. Clone `--mirror` the repository:

   Cloning with the mirror flag will create a bare repository.
   This ensures you get all the branches within the repo.

   It creates a directory called `<repo-name>.git`
   (in our example, `test-git-lfs-repo-migration.git`),
   mirroring the upstream project:

   ```shell
   git clone --mirror git@gitlab.com:gitlab-tests/test-git-lfs-repo-migration.git
   ```

1. Convert the Git history with BFG:

   ```shell
   bfg --convert-to-git-lfs "*.{png,mp4,jpg,gif}" --no-blob-protection test-git-lfs-repo-migration.git
   ```

   It is scanning all the history, and looking for any files with
   that extension, and then converting them to an LFS pointer.

1. Clean up the repository:

   ```shell
   # Change into the mirror repo directory:
   cd test-git-lfs-repo-migration.git

   # Clean up the repo:
   git reflog expire --expire=now --all && git gc --prune=now --aggressive
   ```

   You can also take a look on how to further [clean the repository](https://docs.gitlab.com/ee/user/project/repository/reducing_the_repo_size_using_git.html),
   but it's not necessary for the purposes of this guide.

1. Install Git LFS in the mirror repository:

   ```shell
   git lfs install
   ```

1. [Unprotect the default branch](https://docs.gitlab.com/ee/user/project/protected_branches.html),
   so that we can force-push the rewritten repository:

   1. Navigate to your project's **Settings > Repository** and
   expand **Protected Branches**.
   1. Scroll down to locate the protected branches and click
   **Unprotect** the default branch.

1. Force-push to GitLab:

   ```shell
   git push --force
   ```

1. Track the files you want with LFS:

   ```shell
   # Change into the /tmp directory
   cd /tmp

   # Clone the repo
   git clone git@gitlab.com:gitlab-tests/test-git-lfs-repo-migration.git

   # Change into the upstream repo directory:
   cd test-git-lfs-repo-migration

   # You may need to reset your local copy with upstream's `master` after force-pushing from the mirror:
   git reset --hard origin/master

   # Track the files with LFS:
   git lfs track "*.gif" "*.png" "*.jpg" "*.psd" "*.mp4" "img/"

   # Push up changes to .gitattributes
   git add .gitattributes && git commit -m 'Track .gif,.png,.jpg,.psd,.mp4 and img/' && git push
   ```

   Now all existing the files you converted, as well as the new
   ones you add, will be properly tracked with LFS.

1. [Re-protect the default branch](../../../user/project/protected_branches.md):

   1. Navigate to your project's **Settings > Repository** and
   expand **Protected Branches**.
   1. Select the default branch from the **Branch** dropdown menu,
   and set up the
   **Allowed to push** and **Allowed to merge** rules.
   1. Click **Protect**.
```
